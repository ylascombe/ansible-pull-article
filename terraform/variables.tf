
variable "aws_trigram" {}
variable "aws_keypair" {}
# variable "public_ssh_key" {}

variable "env" {
    default = "demo"
}
variable "git_repository" {
    default = "https://gitlab.com/ylascombe/ansible-pull-article"
}
variable "web_role_secret" {}
variable "db_role_secret" {}